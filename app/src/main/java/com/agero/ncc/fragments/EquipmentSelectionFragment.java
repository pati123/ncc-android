package com.agero.ncc.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.adapter.EquipmentSelectionAdapter;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.model.Equipment;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.Status;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import durdinapps.rxfirebase2.RxFirebaseDatabase;

/**
 * To show Equipment selection screen.
 */
public class EquipmentSelectionFragment extends BaseStatusFragment implements EquipmentSelectionAdapter.OnRecyclerItemClick {
    EquipmentSelectionAdapter equipmentAdapter;
    @BindView(R.id.recycler_equipment)
    RecyclerView mEquipmentRecycler;
    Unbinder unbinder;
    ArrayList<Equipment> equipmentList;
    Equipment mEquipment;
    DatabaseReference myRef, myRef1, mJobReference, mUserEquipmentRef;
    private int mLastClickedEquipment = -1;
    UserError mUserError;
    HomeActivity mHomeActivity;
    private long oldRetryTime = 0;
    private String mUserId;
    private int mServiceUnfinished = 0;

    private JobDetail mCurrentJob;
    private String mUserName;

    private boolean isSaveCalled = false;
    private boolean isFromOnDutyToolbar = false;

    ValueEventListener valueEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (!isSaveCalled) {
                loadEquipmentFromFirebase(dataSnapshot);
                mServiceUnfinished--;
                if (mServiceUnfinished <= 0) {
                    hideProgress();
                    loadAdapter();
                }
            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Equipment Selection Equipment List Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getDataFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    ValueEventListener valueEventUserListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            loadUserData(dataSnapshot);
            mServiceUnfinished--;
            if (mServiceUnfinished <= 0) {
                hideProgress();
                loadAdapter();
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            hideProgress();
            if (isAdded()) {
                if (databaseError != null && databaseError.getMessage() != null) {
                    mHomeActivity.mintlogEvent("Dispatcher Active Jobs User Equipment Firebase DatabaseError - Error code - " + databaseError.getCode() + " Error Message -" + databaseError.getMessage());
                }
                if (oldRetryTime == 0 || (System.currentTimeMillis() - oldRetryTime) > NccConstants.RETRY_LIMIT_MILLI_SEC) {
                    getUserEquipmentFromFirebase();
                    oldRetryTime = System.currentTimeMillis();
                }
            }
        }
    };

    private void loadUserData(DataSnapshot dataSnapshot) {
        if (dataSnapshot != null) {
            try {
                Equipment equipment = dataSnapshot.getValue(Equipment.class);
                if (equipment != null) {
                    mEditor.putString(NccConstants.EQUIPMENT_PREF_KEY, new Gson().toJson(equipment)).commit();
                    mEditor.putString(NccConstants.EQUIPMENT_ID, equipment.getEquipmentId()).commit();
                } else {
                    mEditor.putString(NccConstants.EQUIPMENT_PREF_KEY, new Gson().toJson("")).commit();
                    mEditor.putString(NccConstants.EQUIPMENT_ID, "-1").commit();
                }
            } catch (Exception e) {
                mHomeActivity.mintLogException(e);
                e.printStackTrace();
            }
        }
    }


    public EquipmentSelectionFragment() {
        // Intentionally empty
    }

    public static EquipmentSelectionFragment newInstance(String userId, boolean showOnOffDutyToolbar) {
        EquipmentSelectionFragment fragment = new EquipmentSelectionFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.SIGNIN_USER_ID, userId);
        bundle.putBoolean(NccConstants.SHOW_ON_OFF_DUTY, showOnOffDutyToolbar);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static Fragment newInstance(String userId, String userName, JobDetail mCurrentJob) {
        EquipmentSelectionFragment fragment = new EquipmentSelectionFragment();
        Bundle bundle = new Bundle();
        bundle.putString(NccConstants.SIGNIN_USER_ID, userId);
        bundle.putBoolean(NccConstants.SHOW_ON_OFF_DUTY, false);
        bundle.putString(NccConstants.USERNAME, userName);
        bundle.putString(NccConstants.JOB_DETAIL, new Gson().toJson(mCurrentJob));
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_equipment_selection, fragment_content, true);
        unbinder = ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mUserError = new UserError();
        NCCApplication.getContext().getComponent().inject(this);
        mHomeActivity.showToolbar(getString(R.string.title_select_equipment));
        mHomeActivity.hideBottomBar();
        mHomeActivity.hideSaveButton();
        mEditor = mPrefs.edit();
        isFromOnDutyToolbar = false;
        if (getArguments().containsKey(NccConstants.SHOW_ON_OFF_DUTY)) {
            isFromOnDutyToolbar = getArguments().getBoolean(NccConstants.SHOW_ON_OFF_DUTY);
        }

        mEditor.putBoolean(NccConstants.SHOW_ON_OFF_DUTY, isFromOnDutyToolbar).commit();
        mEquipmentRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));

        mUserId = getArguments().getString(NccConstants.SIGNIN_USER_ID);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Equipment/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        //myRef.keepSynced(true);
        myRef1 = database.getReference("Equipment/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        //myRef1.keepSynced(true);
        mUserEquipmentRef = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mUserId + "/Equipment");
        //mUserEquipmentRef.keepSynced(true);
        if (getArguments().containsKey(NccConstants.USERNAME)) {
            mUserName = getArguments().getString(NccConstants.USERNAME);
            mCurrentJob = new Gson().fromJson(getArguments().getString(NccConstants.JOB_DETAIL), JobDetail.class);
            mJobReference = database.getReference("ActiveJobs/"
                    + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "") + "/" + mCurrentJob.getDispatchId());
            // mJobReference.keepSynced(true);
        }

        if (Utils.isNetworkAvailable()) {

            isSaveCalled = false;
            showProgress();

            getDataFromFirebase();
            getUserEquipmentFromFirebase();

        } else {
            hideProgress();
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

        return superView;
    }

    private void getUserEquipmentFromFirebase() {
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    mServiceUnfinished++;
                    mUserEquipmentRef.addValueEventListener(valueEventUserListener);
                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });
    }

    private void getDataFromFirebase() {
        TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
            @Override
            public void onRefreshSuccess() {
                if (isAdded()) {
                    mServiceUnfinished++;
                    myRef.addValueEventListener(valueEventListener);
                }
            }

            @Override
            public void onRefreshFailure() {
                hideProgress();
                if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                }
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        try {
            myRef.removeEventListener(valueEventListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadEquipmentFromFirebase(DataSnapshot dataSnapshot) {
        equipmentList = new ArrayList<>();
        if (dataSnapshot.hasChildren() && isAdded()) {
            Iterable<DataSnapshot> childList = dataSnapshot.getChildren();
            for (DataSnapshot data : childList) {
                try {
                    Equipment equipmentResults = data.getValue(Equipment.class);
                    if (equipmentResults != null && !TextUtils.isEmpty(equipmentResults.getEquipmentId())) {
                        this.equipmentList.add(equipmentResults);
                    }
                } catch (Exception e) {
                    mHomeActivity.mintLogException(e);
                }
            }

            if (equipmentList != null) {
                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("json", new Gson().toJson(equipmentList));
                mHomeActivity.mintlogEventExtraData("Equipment Selection Equipment List", extraDatas);
            }
        }

    }

    private void loadAdapter() {
        if (equipmentList == null) return;
        Collections.sort(equipmentList, new Comparator<Equipment>() {
            @Override
            public int compare(Equipment equipment, Equipment equipment1) {
                if (equipment.getEquipmentId() != null && equipment1.getEquipmentId() != null && equipment.getIsAvailable() != null && equipment1.getIsAvailable() != null) {
                    if (equipment.getEquipmentId().equalsIgnoreCase(mPrefs.getString(NccConstants.EQUIPMENT_ID, "-1"))) {
                        equipment.setAvailable(true);
                    } else if (equipment1.getEquipmentId().equalsIgnoreCase(mPrefs.getString(NccConstants.EQUIPMENT_ID, "-1"))) {
                        equipment1.setAvailable(true);
                    }
                    int b1 = equipment.getIsAvailable() ? 1 : 0;
                    int b2 = equipment1.getIsAvailable() ? 1 : 0;

                    return b2 - b1;
                }
                return 0;
            }
        });
        equipmentAdapter = new EquipmentSelectionAdapter(getContext(), equipmentList, mPrefs.getString(NccConstants.EQUIPMENT_ID, "-1"), EquipmentSelectionFragment.this);
        if (mEquipmentRecycler != null && equipmentAdapter != null) {
            mEquipmentRecycler.setAdapter(equipmentAdapter);
        }
        if (mLastClickedEquipment > -1 || !mPrefs.getString(NccConstants.EQUIPMENT_ID, "-1").equalsIgnoreCase("-1")) {
            mHomeActivity.saveEnable();
        } else {
            mHomeActivity.saveDisable();
        }
    }

    @Override
    public void onItemClicked(int position) {
        isSaveCalled = true;
        mEquipment = equipmentList.get(position);

        if (mLastClickedEquipment >= 0) {
            View equipmentView = mEquipmentRecycler.getLayoutManager().findViewByPosition(mLastClickedEquipment);
            if(equipmentView != null) {
                ((RadioButton) equipmentView.findViewById(R.id.radioButton_equipment)).setChecked(false);
            }
        } else {
            mLastClickedEquipment = position;
            for (int i = 0; i < equipmentList.size() - 1; i++) {
                View view = mEquipmentRecycler.getLayoutManager().findViewByPosition(i);
                if (view != null) {
                    ((RadioButton) view.findViewById(R.id.radioButton_equipment)).setChecked(false);
                }
            }
        }
        View equipmentView = mEquipmentRecycler.getLayoutManager().findViewByPosition(position);
        if(equipmentView != null) {
            ((RadioButton) equipmentView.findViewById(R.id.radioButton_equipment)).setChecked(true);
        }

        mEditor.putString(NccConstants.EQUIPMENT_PREF_KEY, new Gson().toJson(mEquipment)).commit();
        String id = mEquipment.getEquipmentId();

        String equipmentJson = mPrefs.getString(NccConstants.EQUIPMENT_PREF_KEY, "");

        if (Utils.isNetworkAvailable() && !TextUtils.isEmpty(equipmentJson)) {
            showProgress();
            TokenManager.getInstance().validateToken(mHomeActivity, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    isSaveCalled = true;
                    makeUserOnDuty(id, equipmentJson);
                }

                @Override
                public void onRefreshFailure() {
                    hideProgress();
                    if(isAdded()) {
                    mHomeActivity.tokenRefreshFailed();
                    }
                }
            });
        } else {
            hideProgress();
            mUserError.title = ""; mUserError.message = getString(R.string.network_error_message);
            showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
        }

    }

    private void makeUserOnDuty(String id, String equipmentJson) {
        mHomeActivity.uploadDuty(mUserId, true, null, false, new HomeActivity.DutyUpdateListener() {
            @Override
            public void onSuccess() {
                if(isAdded() && isVisible()) {
                    String oldEquipmentId = mPrefs.getString(NccConstants.EQUIPMENT_ID, "-1");
                    if (!TextUtils.isEmpty(oldEquipmentId) && !"-1".equalsIgnoreCase(oldEquipmentId)) {
                        makeOldEquipmentAvailable(oldEquipmentId, id, equipmentJson);
                    } else if (!TextUtils.isEmpty(id) && !"-1".equalsIgnoreCase(id)) {
                        makeNewEquipmetNotAvailable(id, equipmentJson);
                    } else if (!TextUtils.isEmpty(equipmentJson)) {
                        uploadEquipmentToProfile(equipmentJson);
                    } else if (!TextUtils.isEmpty(mUserName) && mCurrentJob != null) {
                        assignJobToDriver();
                    } else {
                        hideProgress();
                        mHomeActivity.popBackStackImmediate();
                    }
                }
            }

            @Override
            public void onFailure() {
                if(isAdded() && isVisible()){
                    hideProgress();
                    mHomeActivity.popBackStackImmediate();
                }
            }
        });
    }

    private void makeOldEquipmentAvailable(String oldEquipmentId, String newEquipmentId, String equipmentJson) {
        HashMap<String, String> extraDatas = new HashMap<>();
        extraDatas.put("oldequipmentId", oldEquipmentId);
        mHomeActivity.mintlogEventExtraData("Equipment Old", extraDatas);

        HashMap<String, Object> oldUpdate = new HashMap<>();
        oldUpdate.put("isAvailable", true);
        RxFirebaseDatabase.updateChildren(myRef.child(oldEquipmentId), oldUpdate).subscribe(() -> {
            if (isAdded() && isVisible()) {
                if (!TextUtils.isEmpty(newEquipmentId) && !"-1".equalsIgnoreCase(newEquipmentId)) {
                    makeNewEquipmetNotAvailable(newEquipmentId, equipmentJson);
                } else if (!TextUtils.isEmpty(equipmentJson)) {
                    uploadEquipmentToProfile(equipmentJson);
                } else if (!TextUtils.isEmpty(mUserName) && mCurrentJob != null) {
                    assignJobToDriver();
                } else {
                    hideProgress();
                    mHomeActivity.popBackStackImmediate();
                }
            }

        }, throwable -> {
            if (isAdded()) {
                mHomeActivity.mintlogEvent("Equipment Selection Change isAvailable Firebase Database Error - " + throwable.getLocalizedMessage());
            }
            hideProgress();
        });
    }

    private void makeNewEquipmetNotAvailable(String newEquipmentId, String equipmentJson) {

        HashMap<String, Object> newUpdate = new HashMap<>();
        newUpdate.put("isAvailable", false);

        HashMap<String, String> extraDatas = new HashMap<>();
        extraDatas.put("newequipmentId", newEquipmentId);
        mHomeActivity.mintlogEventExtraData("Equipment Selected", extraDatas);

        RxFirebaseDatabase.updateChildren(myRef1.child(newEquipmentId), newUpdate).subscribe(() -> {
            hideProgress();
            if (isAdded() && isVisible()) {
                if (!TextUtils.isEmpty(equipmentJson)) {
                    uploadEquipmentToProfile(equipmentJson);
                } else if (!TextUtils.isEmpty(mUserName) && mCurrentJob != null) {
                    assignJobToDriver();
                } else {
                    hideProgress();
                    mHomeActivity.popBackStackImmediate();
                }
            }
        }, throwable -> {
            if (isAdded()) {
                mHomeActivity.mintlogEvent("Equipment Selection Change isAvailable Firebase Database Error - " + throwable.getLocalizedMessage());
            }
            hideProgress();
        });
    }


    private void uploadEquipmentToProfile(String equipmentJson) {

        mServiceUnfinished++;
        Equipment mEquipment = new Gson().fromJson(equipmentJson, Equipment.class);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reference = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                + "/" + mUserId + "/Equipment");
        //reference.keepSynced(true);
        if (equipmentJson != null) {
            HashMap<String, String> extraDatas = new HashMap<>();
            extraDatas.put("json", equipmentJson);
            mHomeActivity.mintlogEventExtraData("User Profile Equipment Update", extraDatas);
        }
        RxFirebaseDatabase.setValue(reference, mEquipment).subscribe(() -> {
            if (isAdded() && isVisible()) {
                if (!TextUtils.isEmpty(mUserName)) {
                    assignJobToDriver();
                } else {
                    hideProgress();
                    mHomeActivity.popBackStackImmediate();
                }
            }
        }, throwable -> {
            if (isAdded()) {
                mHomeActivity.mintlogEvent("Equipment Selection Upload Equipment Firebase Database Error - " + throwable.getLocalizedMessage());
            }
            hideProgress();
        });


    }

    private void assignJobToDriver() {

        if (mCurrentJob != null) {
            ArrayList<Status> statusHistory = mCurrentJob.getStatusHistory();

            if (statusHistory != null && statusHistory.size() > 0) {
                statusHistory.add(mCurrentJob.getCurrentStatus());
            } else {
                statusHistory = new ArrayList<Status>();
                statusHistory.add(mCurrentJob.getCurrentStatus());
            }


            assignmentAPI(mCurrentJob.getDispatchId(),
                    mUserId,
                    mUserName, mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""),
                    mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""), mCurrentJob.getDispatchAssignedToId(), statusHistory, new StatusUpdateListener() {
                        @Override
                        public void onSuccess() {
                            if (isAdded() && isVisible()) {
                                hideProgress();
                                mHomeActivity.popBackStackImmediate();
                            }
                        }

                        @Override
                        public void onFailure() {
                            if (isAdded() && isVisible()) {
                                hideProgress();
                                mHomeActivity.popBackStackImmediate();
                            }
                        }
                    });
        }else{
            if (isAdded() && isVisible()) {
                hideProgress();
                mHomeActivity.popBackStackImmediate();
            }
        }
    }



   /* private void setIsAvailable(String oldEquipmentId, String newEquipmentId) {

        HashMap<String, Object> oldUpdate = new HashMap<>();
        oldUpdate.put("isAvailable", true);

        HashMap<String, Object> newUpdate = new HashMap<>();
        newUpdate.put("isAvailable", false);

        if (oldEquipmentId != null && !"-1".equalsIgnoreCase(oldEquipmentId)) {
            HashMap<String, String> extraDatas = new HashMap<>();
            extraDatas.put("oldequipmentId", oldEquipmentId);
            mHomeActivity.mintlogEventExtraData("Equipment Old", extraDatas);
        }

        if (newEquipmentId != null && !"-1".equalsIgnoreCase(newEquipmentId)) {
            HashMap<String, String> extraDatas = new HashMap<>();
            extraDatas.put("newequipmentId", newEquipmentId);
            mHomeActivity.mintlogEventExtraData("Equipment Selected", extraDatas);
        }

        if (oldEquipmentId != null && !"-1".equalsIgnoreCase(oldEquipmentId) && newEquipmentId != null && !"-1".equalsIgnoreCase(newEquipmentId)) {
            mServiceUnfinished++;
            RxFirebaseDatabase.updateChildren(myRef.child(oldEquipmentId), oldUpdate).subscribe(() -> {
                myRef1 = myRef1.child(newEquipmentId);
                updateNewHashMap(myRef1, newUpdate);
            }, throwable -> {
                if (isAdded()) {
                    mHomeActivity.mintlogEvent("Equipment Selection Change isAvailable Firebase Database Error - " + throwable.getLocalizedMessage());
                }
                hideProgress();
            });
        }else if(newEquipmentId != null && !"-1".equalsIgnoreCase(newEquipmentId)){
            myRef1 = myRef1.child(newEquipmentId);
            updateNewHashMap(myRef1, newUpdate);
        }


    }

    private void updateNewHashMap(DatabaseReference ref, HashMap<String, Object> newUpdate) {

        RxFirebaseDatabase.updateChildren(ref, newUpdate).subscribe(() -> {
            mServiceUnfinished--;
            if (mServiceUnfinished <= 0) {
                hideProgress();
                if (isAdded() && isVisible()) {
                    if (!TextUtils.isEmpty(mUserName)) {
                        assignJobToDriver();
                    } else {
                        mHomeActivity.popBackStackImmediate();
                    }
                }
            }
        }, throwable -> {
            if (isAdded()) {
                mHomeActivity.mintlogEvent("Equipment Selection Change isAvailable Firebase Database Error - " + throwable.getLocalizedMessage());
            }
            hideProgress();
        });
    }*/


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        try {
            if (unbinder != null) {
                unbinder.unbind();
            }
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
                myRef.removeEventListener(valueEventUserListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
                myRef.removeEventListener(valueEventUserListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (myRef != null) {
                myRef.removeEventListener(valueEventListener);
                myRef.removeEventListener(valueEventUserListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*private void assignJobToDriver() {


        mJobReference.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

//                if (mCurrentJob.getDispatchAssignedToId() == null || mutableData.getValue(JobDetail.class).getDispatchAssignedToId() == null ||
//                        mCurrentJob.getDispatchAssignedToId().equalsIgnoreCase(mutableData.getValue(JobDetail.class).getDispatchAssignedToId())
//                                && !mUserId.equalsIgnoreCase(mCurrentJob.getDispatchAssignedToId())) {
                JobDetail jobDetail = mutableData.getValue(JobDetail.class);


                ArrayList<Status> statusHistoryForUpdate = new ArrayList<>();
                if (mCurrentJob.getStatusHistory() != null) {
                    statusHistoryForUpdate = mCurrentJob.getStatusHistory();
                }

                statusHistoryForUpdate.add(mCurrentJob.getCurrentStatus());


                Status currentStatusForUpdate = new Status();
                currentStatusForUpdate.setAppId(NccConstants.SUB_SOURCE);
                currentStatusForUpdate.setStatusCode(JOB_STATUS_ASSIGNED);
                currentStatusForUpdate.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                currentStatusForUpdate.setUserName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                currentStatusForUpdate.setStatusTime(DateTimeUtils.getUtcTimeFormat());
                currentStatusForUpdate.setServerTimeUtc(DateTimeUtils.getUtcTimeFormat());
                currentStatusForUpdate.setSource("FIREBASE");
                currentStatusForUpdate.setSubSource(NccConstants.SUB_SOURCE);
                currentStatusForUpdate.setDeviceId(Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

                jobDetail.setStatusHistory(statusHistoryForUpdate);
                jobDetail.setCurrentStatus(currentStatusForUpdate);
                jobDetail.setDriverAssignedByName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                jobDetail.setDriverAssignedById(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                jobDetail.setDispatchAssignedToId(mUserId);
                jobDetail.setDispatchAssignedToName(mUserName);

                mutableData.setValue(jobDetail);

                HashMap<String, String> extraDatas = new HashMap<>();
                extraDatas.put("json", new Gson().toJson(jobDetail));
                mHomeActivity.mintlogEventExtraData("Assign driver job", extraDatas);

                return Transaction.success(mutableData);
//                } else {
//                    return Transaction.abort();
//                }

            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

                hideProgress();
                if (isAdded() && isVisible()) {
                    mHomeActivity.popBackStackImmediate();
                }


            }
        });
    }*/
}
