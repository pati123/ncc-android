package com.agero.ncc.fragments;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class NewEquipmentFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener {
    @BindView(R.id.spinner_equipment_type)
    Spinner mSpinnerEquipmentType;
    @BindView(R.id.spinner_licence_plate)
    Spinner mSpinnerLicencePlate;
    @BindView(R.id.edit_equipment_platenumber)
    EditText mEditEquipmentPlatenumber;
    @BindView(R.id.edit_equipment_nickname)
    EditText mEditEquipmentNickname;
    Unbinder unbinder;
    ArrayAdapter<String> stateAdapter;
    TextView spinnerText;
    String mSelectedEquipment;
    HomeActivity mHomeActivity;

    public NewEquipmentFragment() {
        // Intentionally empty
    }

    public static NewEquipmentFragment newInstance() {
        NewEquipmentFragment fragment = new NewEquipmentFragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_add_equipment, fragment_content, true);

        unbinder = ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_new_equipment));
        mHomeActivity.setOnToolbarSaveListener(this);
        spinnerAdapter(stateAdapter, mSpinnerEquipmentType, R.array.equipment_type);
        spinnerAdapter(stateAdapter, mSpinnerLicencePlate, R.array.licence_plate_state);

        mSpinnerEquipmentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adp, View view,
                                       int pos, long arg3) {
                spinnerText = (TextView) mSpinnerEquipmentType.getSelectedView();
                mSelectedEquipment = adp.getSelectedItem().toString();
                enableSave();
                if (pos == 0) {
                    final ColorStateList colors = spinnerText.getHintTextColors();
                    spinnerText.setTextColor(colors);
                } else {
                    spinnerText.setTextColor(Color.BLACK);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mSpinnerLicencePlate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adp, View view,
                                       int pos, long arg3) {
                spinnerText = (TextView) mSpinnerLicencePlate.getSelectedView();
                mSelectedEquipment = adp.getSelectedItem().toString();
                enableSave();

                if (pos == 0) {
                    final ColorStateList colors = spinnerText.getHintTextColors();
                    spinnerText.setTextColor(colors);
                } else {
                    spinnerText.setTextColor(Color.BLACK);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return superView;
    }

    private void spinnerAdapter(ArrayAdapter<String> spinnerArrayAdapter, Spinner mSpinner, int arrays) {
        spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), android.R.layout.simple_spinner_item, getResources().getStringArray(arrays)) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                spinnerText = (TextView) view;
                if (position == 0) {
                    spinnerText.setTextColor(Color.rgb(120, 120, 120));
                } else {
                    spinnerText.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(spinnerArrayAdapter);
    }

    public void enableSave() {
        if (isAdded()) {
            if (mEditEquipmentPlatenumber.getText().toString().trim().isEmpty() || mEditEquipmentNickname.getText().toString().trim().isEmpty() ||
                    mSpinnerLicencePlate.getSelectedItemPosition() <= 0 || mSpinnerEquipmentType.getSelectedItemPosition() <= 0) {
                mHomeActivity.equipmentSave("Unsave");
            } else {
                mHomeActivity.equipmentSave("save");
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onSave() {
        enableSave();
    }
}
