package com.agero.ncc.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.ncc.adapter.AlertAdapter;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.db.database.NccDatabase;
import com.agero.ncc.db.entity.Alert;
import com.agero.ncc.db.viewmodel.AlertListViewModel;
import com.agero.ncc.utils.DateTimeUtils;
import com.nikhilpanju.recyclerviewenhanced.OnActivityTouchListener;
import com.nikhilpanju.recyclerviewenhanced.RecyclerTouchListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * To show Alert Screen
 */
public class AlertFragment extends BaseFragment implements RecyclerTouchListener.RecyclerTouchListenerHelper {

    @BindView(R.id.recycler_alert)
    RecyclerView mRecyclerViewAlert;
    AlertAdapter alertAdapter;
    Unbinder unbinder;
    HomeActivity mHomeActivity;
    List<Alert> alertModelArrayList;
    Date mCurrentDate;
    SimpleDateFormat mSimpleDateparse, mSimpleDateFormat;
    int mOldDatePosition = -1;
    @BindView(R.id.text_alerts)
    TextView mTextAlerts;
    @BindView(R.id.text_alerts_desc)
    TextView mTextAlertsDesc;
    private OnActivityTouchListener touchListener;
    private RecyclerTouchListener onTouchListener;
    @Inject
    NccDatabase nccDatabase;
    AlertListViewModel alertListViewModel;
    View view;
    private boolean isLocalChange = false;
    private String eventAction = "";
    private String eventCategory = NccConstants.FirebaseEventCategory.ALERTS;
    UserError mUserError;

    public AlertFragment() {
        // Intentionally empty
    }

    public static AlertFragment newInstance() {
        AlertFragment fragment = new AlertFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        view = inflater.inflate(R.layout.fragment_alert, fragment_content, true);
        unbinder = ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.showBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_alerts));
        mUserError = new UserError();
        mSimpleDateparse = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.US);
        loadAdapter(alertModelArrayList);
        //Swipe Options For Delete
        if (onTouchListener != null) {
            onTouchListener.setIndependentViews(R.id.image_alert_close)
                    .setClickable(new RecyclerTouchListener.OnRowClickListener() {
                        @Override
                        public void onRowClicked(int position) {
                            eventAction = NccConstants.FirebaseEventAction.VIEW_ALERT_DETAIL;
                            mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
                            Alert alertResult = alertModelArrayList.get(position);
                            if(alertResult.getRead() != null && alertResult.getAlertDescription() != null) {
                                alertResult.setRead(true);
                                nccDatabase.getAlertDao().insertAll(alertResult);
                                isLocalChange = true;
                                alertAdapter.notifyItemChanged(position);
                                mHomeActivity.setBadgeCount(3,mHomeActivity.getUnReadAlertCount());
                            }
                        }

                        @Override
                        public void onIndependentViewClicked(int independentViewID, int position) {
                            AlertDialogFragment alert = AlertDialogFragment.newDialog("", Html.fromHtml("<big><b>" + getString(R.string.dialog_alert_title) + "</b></big>")
                                    , Html.fromHtml("<b>" + getString(R.string.dialog_alert_delete) + "</b>"), Html.fromHtml("<b>" + getString(R.string.dialog_alert_cancel) + "</b>"));
                            alert.setListener(new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    if (i == -1) {
                                        setCurrentOldDatePosition();
                                        //Clear all message at once in Today groups)
                                        if (alertModelArrayList.get(position).getAlertTitle().equalsIgnoreCase("Today") && mOldDatePosition > position &&
                                                alertModelArrayList.get(mOldDatePosition).getAlertTitle().equalsIgnoreCase("Old")) {
                                            if ((mOldDatePosition > position)) {
                                                Alert alert = alertListViewModel.getItemAndPersonList().getValue().get(position + 1);
                                                nccDatabase.getAlertDao().delete(alert);
                                            } else {
                                                Alert alert = alertListViewModel.getItemAndPersonList().getValue().get(position - (alertModelArrayList.size() - alertListViewModel.getItemAndPersonList().getValue().size()));
                                                nccDatabase.getAlertDao().delete(alert);
                                            }
                                        } else if (alertModelArrayList.get(position).getAlertTitle().equalsIgnoreCase("Today")
                                                && mOldDatePosition > -1 && alertModelArrayList.size() > mOldDatePosition) {
                                            nccDatabase.getAlertDao().deleteByCurrentDate(getTodayStartDate(), getTodayEndDate());
                                            mOldDatePosition = -1;
                                        } else {
                                            //Clear all message at once in Old groups
//
                                            if ((mOldDatePosition > -1 && mOldDatePosition + 1 > position)) {
                                                if(position == 0 && mOldDatePosition > 0){
                                                    nccDatabase.getAlertDao().deleteByCurrentDate(getTodayStartDate(),getTodayEndDate());
                                                }else{
                                                    nccDatabase.getAlertDao().deleteByOldDate(getTodayStartDate(), getTodayStartDate());
                                                }
                                            } else {
                                                nccDatabase.getAlertDao().deleteByCurrentDate(getTodayStartDate(),getTodayEndDate());
                                            }
                                             eventAction = NccConstants.FirebaseEventAction.DELETE_ALL;
                                            mHomeActivity.firebaseLogEvent(eventCategory,eventAction);
                                        }


                                    }
                                    mHomeActivity.setBadgeCount(3,mHomeActivity.getUnReadAlertCount());
                                    dialogInterface.dismiss();
                                }
                            });
                            alert.show(getFragmentManager().beginTransaction(), AlertFragment.class.getClass().getCanonicalName());

                        }
                    }).setSwipeOptionViews(R.id.image_alert_swipe_delete)
                    .setSwipeable(R.id.constraint_alerts, R.id.linear_swipe_delete, new RecyclerTouchListener.OnSwipeOptionsClickListener() {
                        @Override
                        public void onSwipeOptionClicked(int viewID, int position) {
                            //delete the individual message in list
                            if(position > -1) {
                                try {
                                    Alert alert = alertListViewModel.getItemAndPersonList().getValue().get(position - (alertModelArrayList.size() - alertListViewModel.getItemAndPersonList().getValue().size()));
                                    nccDatabase.getAlertDao().delete(alert);
                                    mHomeActivity.setBadgeCount(3, mHomeActivity.getUnReadAlertCount());
                                    mOldDatePosition = -1;
                                    eventAction = NccConstants.FirebaseEventAction.DELETE_ONE;
                                    mHomeActivity.firebaseLogEvent(eventCategory, eventAction);
                                } catch (ArrayIndexOutOfBoundsException ae) {
                                      ae.printStackTrace();
                                }
                            }
                        }
                    });
        }

        //LiveData Observer Call when database changes
        alertListViewModel = ViewModelProviders.of(mHomeActivity).get(AlertListViewModel.class);
        alertListViewModel.getItemAndPersonList().observe(mHomeActivity, new Observer<List<Alert>>() {
            @Override
            public void onChanged(@Nullable List<Alert> alerts) {
                if(mHomeActivity != null && isAdded()) {
                    try {
                        mCurrentDate = Calendar.getInstance().getTime();
                        if (!isLocalChange) {
                            if (Utils.isNetworkAvailable()) {
                                loadAdapter(alerts);
                            } else {
                                mUserError.title = "";
                                mUserError.message = getString(R.string.network_error_message);
                                showError(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), mUserError);
                            }
                        }
                        isLocalChange = false;
                    } catch(Exception e) {
                         e.printStackTrace();
                    }
                }
            }
        });

        return superView;
    }

    private Date getTodayStartDate(){
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 1);
        todayStart.set(Calendar.AM_PM, 0);
        return todayStart.getTime();
    }

    private Date getTodayEndDate(){
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR, 11);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.AM_PM, 1);
        return todayEnd.getTime();
    }

    private void loadAdapter(List<Alert> alerts) {
        ButterKnife.bind(this, view);
        mRecyclerViewAlert.setLayoutManager(new LinearLayoutManager(getActivity()));
        onTouchListener = new RecyclerTouchListener(getActivity(), mRecyclerViewAlert);
        mRecyclerViewAlert.addOnItemTouchListener(onTouchListener);
        if (alerts != null) {
            loadAlertFromDataBase(alerts);
        }
        mHomeActivity.setBadgeCount(3,mHomeActivity.getUnReadAlertCount());

    }

    @Override
    public void setOnActivityTouchListener(OnActivityTouchListener listener) {
        this.touchListener = listener;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void loadAlertFromDataBase(List<Alert> alert) {
        alertModelArrayList = alert;
        if (alertModelArrayList != null) {
            orderAlerts();
        }
    }

    private void orderAlerts() {
        setOldDatePosition();
        if (alertModelArrayList.size() > 0 && alertModelArrayList.get(0).getUpdatedTime() != null) {
            if (mSimpleDateFormat.format(mCurrentDate).equalsIgnoreCase(mSimpleDateFormat.format(alertModelArrayList.get(0).getUpdatedTime()))) {
                Alert alertResult = new Alert();
                alertResult.setAlertTitle(DateTimeUtils.getDisplayTimeForHistory(mSimpleDateFormat.format(mCurrentDate)));
                alertModelArrayList.add(0, alertResult);
                if (mOldDatePosition > -1) {
                    Alert alertResult1 = new Alert();
                    alertResult1.setAlertTitle("Old");
                    try {
                        alertModelArrayList.add(mOldDatePosition, alertResult1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else {
                Alert alertResult1 = new Alert();
                alertResult1.setAlertTitle("Old");
                alertModelArrayList.add(0, alertResult1);
            }
        }

        if (alertModelArrayList != null) {
            alertAdapter =
                    new AlertAdapter(getActivity(), alertModelArrayList);
            mRecyclerViewAlert.setAdapter(alertAdapter);
        }
        if (alertModelArrayList.size() > 0) {
            mTextAlerts.setVisibility(View.GONE);
            mTextAlertsDesc.setVisibility(View.GONE);
            mRecyclerViewAlert.setVisibility(View.VISIBLE);
        } else {
            mTextAlerts.setVisibility(View.VISIBLE);
            mTextAlertsDesc.setVisibility(View.VISIBLE);
            mRecyclerViewAlert.setVisibility(View.GONE);
        }

    }

    private void setOldDatePosition() {
        for (int i = 0; i < alertModelArrayList.size(); i++) {
            if (alertModelArrayList.get(i).getUpdatedTime() != null) {
                if (!mSimpleDateFormat.format(mCurrentDate).equalsIgnoreCase(mSimpleDateFormat.format(alertModelArrayList.get(i).getUpdatedTime()))) {
                    mOldDatePosition = i + 1;
                    return;
                }
            }

        }
    }

    private int setCurrentOldDatePosition() {
        for (int i = 0; i < alertModelArrayList.size(); i++) {
            if (alertModelArrayList.get(i).getAlertTitle().equalsIgnoreCase("Old")) {
                return mOldDatePosition = i;
            }
        }
        return 0;
    }

}
