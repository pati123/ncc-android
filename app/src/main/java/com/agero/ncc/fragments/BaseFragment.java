package com.agero.ncc.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.agero.ncc.R;
import com.agero.ncc.activities.BaseActivity;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.UiUtils;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;

import javax.inject.Inject;

public class BaseFragment extends co.chatsdk.ui.main.BaseFragment {

    public static boolean sDisableFragmentAnimations = false;
    @Inject
    public SharedPreferences mPrefs;
    public SharedPreferences.Editor mEditor;
    private View fragLoader;

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        UiUtils.hideKeyboard(getActivity());
        hideProgress();
    }



    public boolean onBackPressed() {
        return false;
    }

    public void showProgress() {
        /*BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.showProgress();
            activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }*/
        if (fragLoader != null) {
            fragLoader.bringToFront();
            fragLoader.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i("*****", "ATTACH :" + getClass().getSimpleName());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        Log.i("*****", "CREATE_VIEW :" + getClass().getSimpleName());
        View view = inflater.inflate(R.layout.base_fragment_overlay, container, false);
        fragLoader = view.findViewById(R.id.fragLoader);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        String className = getClass().getSimpleName();

        String analyticsScreenName = Utils.getScreenNameForAnalytics(getActivity(), className);
        if (!analyticsScreenName.isEmpty()) {
            ((BaseActivity) getActivity()).updateAnalyticsCurrentScreen(analyticsScreenName);
        }
        if (isAdded()) {
            Intent gcm_rec = new Intent(NccConstants.ALERT_ACTION);
            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(gcm_rec);
        }
        UiUtils.hideKeyboard(getActivity());
    }

    @Override
    public void clearData() {

    }

    @Override
    public void reloadData() {

    }

    public void hideProgress() {
        /*BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.hideProgress();
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }*/
        if (fragLoader != null) {
            fragLoader.setVisibility(View.GONE);
        }
    }

    public void showException(Throwable e) {
        showException(BaseActivity.Companion.getERROR_SHOW_TYPE_DIALOG(), e);
    }

    public void showException(int type, Throwable e) {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.showException(type, e);
        }
    }

    public void handleError(Throwable exception) {
        BaseActivity activity = (BaseActivity) getActivity();
        if (activity != null) {
            activity.handleError(exception);
        }
    }

    public void showError(int errorType, UserError error) {
        if(getActivity() != null) {
            ((BaseActivity) getActivity()).showError(errorType, error,null);
        }
    }

    public void showError(int errorType, UserError error, DialogInterface.OnClickListener errorOkCancelCallback) {
        if(getActivity() != null) {
            ((BaseActivity) getActivity()).showError(errorType, error, errorOkCancelCallback);
        }
    }

    DialogInterface.OnClickListener dialogNavigateListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
         if(getActivity() instanceof HomeActivity){
             ((HomeActivity) getActivity()).popBackStackImmediate();
         }
        }
    };

    public void showMessage(String message) {
        if(getActivity() != null) {
            ((BaseActivity) getActivity()).showMessage(message);
        }
    }

    public void displaySingleChoiceAlertDialog(String title, String[] choiceItems, String paginationText,
                                               String positiveButtonText, OnSingleChoiceListener onSingleChoiceListener, int options) {
        final int[] choicePosition = new int[1];
        AlertDialog.Builder builderCompanySelection = new AlertDialog.Builder(getActivity());
        builderCompanySelection.setTitle(title);
        builderCompanySelection.setSingleChoiceItems(choiceItems, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                choicePosition[0] = i;
            }
        });
        builderCompanySelection.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onSingleChoiceListener.onChoiceClicked(choicePosition[0], options);
            }
        });
        builderCompanySelection.setNegativeButton(getString(R.string.cancel), null);
        builderCompanySelection.setNeutralButton(paginationText, null);

        AlertDialog alertDialog = builderCompanySelection.create();
        alertDialog.show();
        if (paginationText != null && !paginationText.isEmpty()) {
            Button alertNegativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL);
            alertNegativeButton.setTextColor(getResources().getColor(R.color.ncc_gray_999));
            alertNegativeButton.setAllCaps(false);
        }
    }

    public void showJobCompleteDialog() {
        FragmentManager manager = getFragmentManager();
        CustomDialogFragment mydialog = CustomDialogFragment.newInstance();
        mydialog.show(manager, "mydialog");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideProgress();
        Log.i("*****", "DESTROY :" + getClass().getSimpleName());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i("*****", "DETACH :" + getClass().getSimpleName());
        hideProgress();
    }

    public interface OnSingleChoiceListener {
        void onChoiceClicked(int position, int options);
    }
}
