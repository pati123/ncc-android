package com.agero.ncc.model

data class StatusUpdateModel(
        var serverTimeUtc: String?,
        var statusCode: String?,
        var dispatchId: String?,
        var isPending: Boolean? = false,
        var userName: String?,
        var userId: String?,
        var authToken: String?,
        var statusTime: String?,
        var source: String?,
        var subSource: String?,
        var deviceId: String?,
        var statusHistory: ArrayList<Status>? = null,
        var dispatchAssignedToId: String?,
        var dispatchAssignedToName: String?,
        var driverAssignedById: String?,
        var driverAssignedByName: String?

)


data class AssignmentModel(
        var serverTimeUtc: String?,
        var dispatchId: String?,
        var userName: String?,
        var userId: String?,
        var authToken: String?,
        var statusTime: String?,
        var source: String?,
        var subSource: String?,
        var deviceId: String?,
        var dispatchAssignedToId: String?,
        var dispatchAssignedToName: String?,
        var driverAssignedById: String?,
        var driverAssignedByName: String?,
        var previousDispatchAssignedToId: String?

)


data class DisablementLocationUpdateModel(
        var disablementLocation: DisablementLocation? = null,
        var authToken: String?,
        var serverTimeUtc: String?,
        var subSource: String?,
        var dispatchId: String?,
        var userId: String?
)

data class TowLocationUpdateModel(
        var towLocation: TowLocation? = null,
        var authToken: String?,
        var serverTimeUtc: String?,
        var subSource: String?,
        var dispatchId: String?,
        var userId: String?
)

