package com.agero.ncc.model

data class Facility (
    var addressList: List<AddressList>? = null,
    var partnerAgreementAcceptance: ArrayList<Any>? = null,
    var partnerAgreementDocument: ArrayList<Any>? = null,
    var overnightJobOfferSettings: OvernightJobOfferSettings? = null,
    var facilityId: String? = null,
    var level: String? = null,
    var serviceRadius: Double? = null,
    var createdAt: String? = null,
    var createdBy: String? = null,
    var lastUpdatedBy: String? = null,
    var lastUpdatedAt: String? = null,
    var lastUpdateImpersonated: Boolean? = null,
    var lastUpdateImpersonator: String? = null,
    var businessName: String? = null,
    var dispatchPhone: String? = null,
//    var agreementGuarantee: AgreementGuarantee? = null,
    var jobPreference: String? = null,
    var showCostToDispatcher: Boolean? = null,
    var showCostToDriver: Boolean? = null,
    var status: String? = null,
    var timeZone: String? = null

)

data class OvernightJobOfferSettings (

    var communicationMethod: String? = null,
    var alternativePhoneSettings: AlternativePhoneSettings? = null

)

data class AddressList (

    var addressType: String? = null,
    var city: String? = null,
    var state: String? = null,
    var address1: String? = null,
    var address2: String? = null,
    var country: String? = null,
    var description: String? = null,
    var name: String? = null,
    var postalCode: String? = null,
    var latitude: Double? = null,
    var longitude: Double? = null

)

data class AlternativePhoneSettings (

    var startTime: String? = null,
    var endTime: String? = null,
    var phoneNumber: String? = null,
    var isCallService: Boolean? = null

)

