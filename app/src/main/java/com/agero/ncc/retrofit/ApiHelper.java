package com.agero.ncc.retrofit;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.agero.ncc.utils.DateTimeUtils;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiHelper {

    @Inject
    public SharedPreferences mPrefs;
    private String mBearerToken;
    private static String BASE_URL = "https://testbedapp.giftbit.com/papi/v1/";
    private static final String TAG = "ApiHelper";

    public ApiHelper(String mBearerToken) {
        this.mBearerToken = mBearerToken;
    }

    Interceptor interceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {

            Request original = chain.request();
            Request request = chain.request()
                    .newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Accept", "application/json")
                    .addHeader("Authorization", "Bearer I6DT4QIfnh7zNc9ijYU4JS7V3A5db6MH")
                    .addHeader("X-REQ-LOCAL-TIME", DateTimeUtils.getReqCurrentDate())
                    .addHeader("X-APP-VER", "1.0")
                    .addHeader("X-APP-OS", "A")
                    .method(original.method(), original.body())
                    .build();
            //long t1 = System.nanoTime();
            //Log.d(TAG, String.format("Sending request %s on %s%n%s", request.url(), chain.connection(),
            //    request.headers()));
            okhttp3.Response response = chain.proceed(request);
            //long t2 = System.nanoTime();
            //Log.d(TAG, String.format("Received response for %s in %.1fms%n%s", response.request().url(),
            //    (t2 - t1) / 1e6d, response.headers()));
            String responseBody = response.body().string();
            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), responseBody))
                    .build();
        }
    };


    Interceptor Pilotinterceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {

            Request original = chain.request();

            Request.Builder builder = chain.request().newBuilder();

            if (!TextUtils.isEmpty(mBearerToken)) {
                builder.addHeader("Authorization", "Bearer " + mBearerToken);
            } else {
                builder.addHeader("project", "mileup");
            }
            builder.method(original.method(), original.body());

            Request request = builder.build();
            //long t1 = System.nanoTime();
            //Log.d(TAG, String.format("Sending request %s on %s%n%s", request.url(), chain.connection(),
            //    request.headers()));
            okhttp3.Response response = chain.proceed(request);
            //long t2 = System.nanoTime();
            //Log.d(TAG, String.format("Received response for %s in %.1fms%n%s", response.request().url(),
            //    (t2 - t1) / 1e6d, response.headers()));
            String responseBody = response.body().string();
            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), responseBody))
                    .build();
        }
    };

    private Retrofit getRetrofit() {
        Retrofit.Builder builder =
                new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create());
        return builder.build();
    }
}
