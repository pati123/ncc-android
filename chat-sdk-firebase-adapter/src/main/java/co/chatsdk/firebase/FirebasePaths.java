/*
 * Created by Itzik Braun on 12/3/2015.
 * Copyright (c) 2015 deluge. All rights reserved.
 *
 * Last Modification at: 3/12/15 4:34 PM
 */

package co.chatsdk.firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import co.chatsdk.core.dao.Keys;
import co.chatsdk.core.session.ChatSDK;

public class FirebasePaths{

    public static final String UsersPath = "users";
    public static final String MessagesPath = "messages";
    public static final String ThreadsPath = "threads";
    public static final String PublicThreadsPath = "threads";
    public static final String DetailsPath = "details";
//    public static final String IndexPath = "searchIndex";
    public static final String OnlinePath = "online";
    public static final String MetaPath = "meta";
    public static final String FollowersPath = "followers";
    public static final String FollowingPath = "follows";
    public static final String Image = "imaeg";
    public static final String Thumbnail = "thumbnail";
    public static final String UpdatedPath = "updated";
    public static final String LastMessagePath = "lastMessage";
    public static final String TypingPath = "typing";
    public static final String ReadPath = Keys.Read;
    public static final String LocationPath = "location";


    /* Not sure if this the wanted implementation but its give the same result as the objective-C code.*/
    /** @return The main databse ref.*/

    public static DatabaseReference firebaseRawRef() {
        return FirebaseDatabase.getInstance().getReference();
    }

    public static DatabaseReference firebaseChatRootRef () {
        return firebaseRawRef().child("Chat").child(ChatSDK.config().firebaseRootPath);
    }

//    public static DatabaseReference firebaseUserRootRef () {
//        return firebaseRawRef().child("Users").child(ChatSDK.config().firebaseRootPath);
//    }

    /* Users */
    /** @return The users main ref.*/
    public static DatabaseReference usersRef(){
        return firebaseRawRef().child("Users").child(ChatSDK.config().firebaseRootPath);
    }

    /** @return The user ref for given id.*/
    public static DatabaseReference userRef(String firebaseId){
        firebaseId =  firebaseId.replace("%7C","|");
        return usersRef().child(firebaseId);
    }

    /** @return The user threads ref.*/
    public static DatabaseReference userThreadsRef(String firebaseId){
        firebaseId =  firebaseId.replace("%7C","|");
        return usersRef().child(firebaseId).child(ThreadsPath);
    }

    /** @return The user meta ref for given id.*/
    public static DatabaseReference userMetaRef(String firebaseId){
        firebaseId =  firebaseId.replace("%7C","|");
        return usersRef().child(firebaseId).child(MetaPath);
    }

    public static DatabaseReference userOnlineRef(String firebaseId){
        firebaseId = firebaseId.replace("%7C","|");
        return userRef(firebaseId).child(OnlinePath);
    }

    public static DatabaseReference userFollowingRef(String firebaseId){
        return userRef(firebaseId).child(FollowingPath);
    }

    public static DatabaseReference userFollowersRef(String firebaseId){
        return userRef(firebaseId).child(FollowersPath);
    }

    /* Threads */
    /** @return The thread main ref.*/
    public static DatabaseReference threadRef(){
        return firebaseRawRef().child("Chat").child(ChatSDK.config().firebaseRootPath).child(ThreadsPath);
    }

    /** @return The thread ref for given id.*/
    public static DatabaseReference threadRef(String firebaseId){
        firebaseId =  firebaseId.replace("%7C","|");
        return threadRef().child(firebaseId);
    }

    public static DatabaseReference threadUsersRef(String firebaseId){
        firebaseId =  firebaseId.replace("%7C","|");
        return threadRef().child(firebaseId).child(UsersPath);
    }

    public static DatabaseReference threadDetailsRef(String firebaseId){
        return threadRef().child(firebaseId).child(DetailsPath);
    }

    public static DatabaseReference threadLastMessageRef(String firebaseId){
        return threadRef().child(firebaseId).child(LastMessagePath);
    }

    public static DatabaseReference threadMessagesRef(String firebaseId){
        return threadRef(firebaseId).child(MessagesPath);
    }

    public static DatabaseReference threadMetaRef(String firebaseId){
        return threadRef(firebaseId).child(MetaPath);
    }

    public static DatabaseReference publicThreadsRef(){
        return firebaseRawRef().child("Chat").child(ChatSDK.config().firebaseRootPath).child(PublicThreadsPath);
    }

    public static DatabaseReference onlineRef(String userEntityID){
        return firebaseRawRef().child("Chat").child(ChatSDK.config().firebaseRootPath).child(OnlinePath).child(userEntityID);
    }


    /* Index */
//    public static DatabaseReference indexRef(){
//        return firebaseRawRef().child("Chat").child(ChatSDK.config().firebaseRootPath).child(IndexPath);
//    }

}
